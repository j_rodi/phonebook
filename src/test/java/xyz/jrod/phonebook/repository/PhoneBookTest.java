package xyz.jrod.phonebook.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import xyz.jrod.phonebook.model.domain.Customer;
import xyz.jrod.phonebook.model.domain.PhoneNumber;
import xyz.jrod.phonebook.model.message.PhoneNumberDto;
import xyz.jrod.phonebook.services.ApiService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static xyz.jrod.phonebook.config.AppStartupRunner.CUST1_ID;
import static xyz.jrod.phonebook.config.AppStartupRunner.CUST2_ID;
import static xyz.jrod.phonebook.config.AppStartupRunner.PH_NUM_NOT_ACTIVE_ID;

/**
 * @author Jason Rodrigues
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class PhoneBookTest {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ApiService apiService;

//    List<PhoneNumber> numberList = new ArrayList<>();
//    List<PhoneNumber> numberList2 = new ArrayList<>();
//
//    private static final int CUST1_ID = 1;
//    private static final int CUST2_ID = 2;
//    private static final int PH_NUM_NOT_ACTIVE_ID = 555;
//
//
//    @Before
//    public void setUp() {
//
//
//        numberList.add(new PhoneNumber("04111111111"));
//        numberList.add(new PhoneNumber("04222222222"));
//        numberList.add(new PhoneNumber("04333333333"));
//        numberList.add(new PhoneNumber("04444444444"));
//
//        numberList2.add(new PhoneNumber("04555555555"));
//        numberList2.add(new PhoneNumber("04666666666"));
//        numberList2.add(new PhoneNumber("04777777777"));
//        numberList2.add(new PhoneNumber("04888888888"));
//
//        // add one to instance to check if activate number works
//        PhoneNumber phoneNumberNotActivated = new PhoneNumber("04999999999");
//        phoneNumberNotActivated.setId(PH_NUM_NOT_ACTIVE_ID);
//        numberList2.add(phoneNumberNotActivated);
//
//        Customer customer1 = new Customer("Jason", "Rodrigues");
//        customer1.setId(CUST1_ID);
//        customer1.setPhoneNumbers(numberList);
//
//        Customer customer2 = new Customer("Bob", "Smith");
//        customer2.setId(CUST2_ID);
//        customer2.setPhoneNumbers(numberList2);
//
//
//        customerRepository.save(customer1);
//        customerRepository.save(customer2);
//
//    }

    /**
     * Test of getAllPhoneNumbers method, of class PhoneNumberRepository.
     */
    @Test
    public void getAllPhoneNumbers() {

        int numPhoneNumbersInserted = 9;

        List<PhoneNumberDto> phoneNumbers = apiService.getAllPhoneNumbers();

        assertNotNull(phoneNumbers);
        assertEquals(numPhoneNumbersInserted, phoneNumbers.size());
    }


    /**
     * Test of get customer phone numbers, apiService->customerService
     */
//    @Test
//    public void getCustomerPhoneNumbers() {
//
//        List<PhoneNumberDto> customer1PhNums = apiService.getPhoneNumbersByCustomerId(CUST1_ID);
//        List<PhoneNumberDto> customer2PhNums = apiService.getPhoneNumbersByCustomerId(CUST2_ID);
//
//        assertNotNull(customer1PhNums);
//        assertNotNull(customer2PhNums);
//
//        assertEquals(4,customer1PhNums.size());
//        assertEquals(5, customer2PhNums.size());
//    }


    /**
     * Test of activatePhoneNumber method, of class ApiService->PhoneNumberService.
     */
//    @Test
//    public void testActivatePhoneNumber() throws Exception {
//
//
//        PhoneNumberDto phoneNumber = apiService.getPhoneNumberById(PH_NUM_NOT_ACTIVE_ID);
//
//        assertNotNull(phoneNumber);
//
//        assertEquals(false, phoneNumber.getActivated());
//
//        apiService.activatePhoneNumber(PH_NUM_NOT_ACTIVE_ID);
//
//        phoneNumber = apiService.getPhoneNumberById(PH_NUM_NOT_ACTIVE_ID);
//
//        assertEquals(true, phoneNumber.getActivated());
//    }




    /**
     * Test of getCustomerById method, of class CustomerRepository.
     */
    @Test
    public void getCustomerById() {

        Integer customerId = 1;
        Customer result = customerRepository.getById(customerId);
        assertNotNull(result);
        assertEquals(customerId, result.getId());
    }


}
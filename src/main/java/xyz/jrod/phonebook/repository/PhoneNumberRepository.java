package xyz.jrod.phonebook.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import xyz.jrod.phonebook.model.domain.PhoneNumber;

import java.util.List;

@Repository
public interface PhoneNumberRepository extends JpaRepository<PhoneNumber, Integer> {

    List<PhoneNumber> getPhoneNumberByCustomerId(Integer customerId);

    PhoneNumber getPhoneNumberById(Integer phoneNumberId);
}

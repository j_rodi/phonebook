package xyz.jrod.phonebook.config;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import xyz.jrod.phonebook.model.domain.Customer;
import xyz.jrod.phonebook.model.domain.PhoneNumber;
import xyz.jrod.phonebook.repository.CustomerRepository;

import java.util.ArrayList;
import java.util.List;


@Component
public class AppStartupRunner implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(AppStartupRunner.class);
    List<PhoneNumber> numberList = new ArrayList<>();
    List<PhoneNumber> numberList2 = new ArrayList<>();

    public static final int CUST1_ID = 1;
    public static final int CUST2_ID = 2;
    public static final int PH_NUM_NOT_ACTIVE_ID = 555;

    @Autowired
    CustomerRepository customerRepository;

    @Override
    public void run(ApplicationArguments args) {

        numberList.add(new PhoneNumber("04111111111"));
        numberList.add(new PhoneNumber("04222222222"));
        numberList.add(new PhoneNumber("04333333333"));
        numberList.add(new PhoneNumber("04444444444"));

        numberList2.add(new PhoneNumber("04555555555"));
        numberList2.add(new PhoneNumber("04666666666"));
        numberList2.add(new PhoneNumber("04777777777"));
        numberList2.add(new PhoneNumber("04888888888"));

        // add one to instance to check if activate number works
        PhoneNumber phoneNumberNotActivated = new PhoneNumber("04999999999");
        phoneNumberNotActivated.setId(PH_NUM_NOT_ACTIVE_ID);
        numberList2.add(phoneNumberNotActivated);

        Customer customer1 = new Customer("Jason", "Rodrigues");
        customer1.setId(CUST1_ID);
        customer1.setPhoneNumbers(numberList);

        Customer customer2 = new Customer("Bob", "Smith");
        customer2.setId(CUST2_ID);
        customer2.setPhoneNumbers(numberList2);


        customerRepository.save(customer1);
        customerRepository.save(customer2);
    }


}
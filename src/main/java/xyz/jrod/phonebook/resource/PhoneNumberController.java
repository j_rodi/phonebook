package xyz.jrod.phonebook.resource;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xyz.jrod.phonebook.model.message.PhoneNumberDto;
import xyz.jrod.phonebook.services.ApiService;

import java.util.List;

/**
 * Created by Jason Rodrigues on 06/03/2019.
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/phoneNumbers")
public class PhoneNumberController {

    @Autowired
    private ApiService apiService;

    @GetMapping
    public List<PhoneNumberDto> getAllPhoneNumbers() {

        return apiService.getAllPhoneNumbers();
    }

    @GetMapping("/{phoneNumberId}")
    public PhoneNumberDto getPhoneNumberById(@PathVariable("phoneNumberId") Integer phoneNumberId) throws Exception {

        return apiService.getPhoneNumberById(phoneNumberId);
    }

    @GetMapping("/customer/{customerId}")
    public List<PhoneNumberDto> getPhoneNumbersByCustomerId(@PathVariable("customerId") Integer customerId) {

        return apiService.getPhoneNumbersByCustomerId(customerId);
    }

    @PatchMapping("/{phoneNumberId}/activate")
    public PhoneNumberDto activatePhoneNumber(@PathVariable("phoneNumberId") Integer phoneNumberId) throws Exception {

        return apiService.activatePhoneNumber(phoneNumberId);
    }


}
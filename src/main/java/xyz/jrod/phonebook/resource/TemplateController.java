package xyz.jrod.phonebook.resource;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@CrossOrigin(origins = "*")
public class TemplateController {


    @RequestMapping("/")
    public String getView(Model model) {


        return "index";

    }


}

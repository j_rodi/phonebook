package xyz.jrod.phonebook.resource;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xyz.jrod.phonebook.model.message.CustomerDto;
import xyz.jrod.phonebook.services.ApiService;

import java.util.List;

/**
 * Created by Jason Rodrigues on 06/03/2019.
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/customers")
public class CustomerController {

    @Autowired
    private ApiService apiService;

    @GetMapping("/")
    public List<CustomerDto> getCustomers() {

        return apiService.getCustomers();
    }

    @GetMapping("/{customerId}")
    public CustomerDto getCustomerById(@PathVariable("customerId") Integer customerId) {

        return apiService.getCustomerById(customerId);
    }

}
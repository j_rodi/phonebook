package xyz.jrod.phonebook.model.message;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PhoneNumberDto {
    private Integer id;
    private String phoneNumber;
    private Boolean activated = false;
    private Integer customerId;
}

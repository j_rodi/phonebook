package xyz.jrod.phonebook.model.message;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CustomerDto {

    private Integer id;
    private String firstname;
    private String lastname;

    private List<PhoneNumberDto> phoneNumbers;

}

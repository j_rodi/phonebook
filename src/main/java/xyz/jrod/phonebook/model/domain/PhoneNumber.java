package xyz.jrod.phonebook.model.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class PhoneNumber {

    @Id
    @GeneratedValue
    private Integer id;

    private String phoneNumber;
    Boolean activated = false;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id")
    private Customer customer;

    public PhoneNumber() {
    }

    public PhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PhoneNumber)) return false;
        return id != null && id.equals(((PhoneNumber) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }
}

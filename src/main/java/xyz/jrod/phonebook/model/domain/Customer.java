package xyz.jrod.phonebook.model.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
public class Customer {

    @Id
    @GeneratedValue
    private Integer id;
    private String firstname;
    private String lastname;


    @OneToMany(
            mappedBy = "customer",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<PhoneNumber> phoneNumbers = new ArrayList<>();

    public Customer(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public Customer() {
    }

    public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {

        this.phoneNumbers = phoneNumbers;

        for (PhoneNumber phoneNumber : this.phoneNumbers) {
            phoneNumber.setCustomer(this);
        }
    }

}

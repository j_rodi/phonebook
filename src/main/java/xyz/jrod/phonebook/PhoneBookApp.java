package xyz.jrod.phonebook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication
public class PhoneBookApp {

    public static void main(String[] args) {
        SpringApplication.run(PhoneBookApp.class, args);
    }
}
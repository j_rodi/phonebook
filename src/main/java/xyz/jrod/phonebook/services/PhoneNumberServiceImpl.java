package xyz.jrod.phonebook.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.jrod.phonebook.model.domain.PhoneNumber;
import xyz.jrod.phonebook.repository.PhoneNumberRepository;

import java.util.List;

/**
 * Created by Jason Rodrigues on 07/03/2019.
 */

@Service
public class PhoneNumberServiceImpl implements PhoneNumberService {

    @Autowired
    private PhoneNumberRepository phoneNumberRepository;

    @Override
    public List<PhoneNumber> getPhoneNumbers() {
        return phoneNumberRepository.findAll();
    }

    @Override
    public PhoneNumber activatePhoneNumber(Integer phoneNumberId) throws Exception {
        PhoneNumber phoneNumber = getPhoneNumberById(phoneNumberId);

        phoneNumber.setActivated(true);

        return savePhoneNumber(phoneNumber);
    }

    public PhoneNumber savePhoneNumber(PhoneNumber phoneNumber) {

        return phoneNumberRepository.save(phoneNumber);
    }

    @Override
    public PhoneNumber getPhoneNumberById(Integer phoneNumberId) throws Exception {

        PhoneNumber phoneNumber = phoneNumberRepository.getPhoneNumberById(phoneNumberId);

        if (phoneNumber == null)
            throw new Exception("Phone Number could not be found");

        return phoneNumber;
    }

    @Override
    public List<PhoneNumber> getPhoneNumbersByCustomerId(Integer customerId) {
        return phoneNumberRepository.getPhoneNumberByCustomerId(customerId);
    }
}

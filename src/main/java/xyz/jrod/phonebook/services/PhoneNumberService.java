package xyz.jrod.phonebook.services;

import org.springframework.stereotype.Service;
import xyz.jrod.phonebook.model.domain.PhoneNumber;

import java.util.List;

/**
 * Created by Jason Rodrigues on 06/03/2019.
 */

@Service
public interface PhoneNumberService {


    List<PhoneNumber> getPhoneNumbers();

    PhoneNumber activatePhoneNumber(Integer phoneNumberId) throws Exception;

    PhoneNumber getPhoneNumberById(Integer phoneNumberId) throws Exception;

    List<PhoneNumber> getPhoneNumbersByCustomerId(Integer customerId);

    PhoneNumber savePhoneNumber(PhoneNumber phoneNumber);

}

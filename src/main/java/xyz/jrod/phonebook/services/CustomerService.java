package xyz.jrod.phonebook.services;

import org.springframework.stereotype.Service;
import xyz.jrod.phonebook.model.domain.Customer;

/**
 * Created by Jason Rodrigues on 06/03/2019.
 */

@Service
public interface CustomerService {

    Customer getCustomerById(Integer customerId);
}

package xyz.jrod.phonebook.services;

import org.springframework.stereotype.Service;
import xyz.jrod.phonebook.model.message.CustomerDto;
import xyz.jrod.phonebook.model.message.PhoneNumberDto;

import java.util.List;

/**
 * Created by Jason Rodrigues on 06/03/2019.
 */
@Service
public interface ApiService {

    /**
     * Returns a List of all customer DTO objects
     *
     * @return customer object list
     */
    List<CustomerDto> getCustomers();

    /**
     * Returns a Customer DTO object
     *
     * @param customerId customer Id number
     * @return customer object
     */
    CustomerDto getCustomerById(Integer customerId);

    /**
     * Returns a list of all phone Numbers in system
     *
     * @return List of phoneNumberDto objects
     */
    List<PhoneNumberDto> getAllPhoneNumbers();

    /**
     * Returns a phoneNumberDto object
     *
     * @param phoneNumberId phone number record id
     * @return a single phoneNumberDto object
     */
    PhoneNumberDto getPhoneNumberById(Integer phoneNumberId) throws Exception;

    /**
     * Returns a list of all phone Numbers in system for a single customer
     *
     * @param customerId phone number record id
     * @return a list of phoneNumberDto object for the customer
     */
    List<PhoneNumberDto> getPhoneNumbersByCustomerId(Integer customerId);

    /**
     * Activates a phone number by supplying phoneNumberId
     *
     * @param phoneNumberId phone number record id
     * @return a single phoneNumberDto object
     */
    PhoneNumberDto activatePhoneNumber(Integer phoneNumberId) throws Exception;
}

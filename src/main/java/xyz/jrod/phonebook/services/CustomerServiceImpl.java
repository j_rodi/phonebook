package xyz.jrod.phonebook.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.jrod.phonebook.model.domain.Customer;
import xyz.jrod.phonebook.repository.CustomerRepository;

/**
 * Created by Jason Rodrigues on 07/03/2019.
 */

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public Customer getCustomerById(Integer customerId) {
        return customerRepository.getById(customerId);
    }
}

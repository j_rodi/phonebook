package xyz.jrod.phonebook.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.jrod.phonebook.model.domain.Customer;
import xyz.jrod.phonebook.model.domain.PhoneNumber;
import xyz.jrod.phonebook.model.message.CustomerDto;
import xyz.jrod.phonebook.model.message.PhoneNumberDto;

import java.util.ArrayList;
import java.util.List;

@Service
public class ApiServiceImpl implements ApiService {

    @Autowired
    private PhoneNumberService phoneNumberService;

    @Autowired
    private CustomerService customerService;

    @Override
    public List<CustomerDto> getCustomers() {
        return null;
    }

    @Override
    public CustomerDto getCustomerById(Integer customerId) {

        Customer customer = customerService.getCustomerById(customerId);

        return toDto(customer);
    }

    @Override
    public List<PhoneNumberDto> getAllPhoneNumbers() {

        return toDto(phoneNumberService.getPhoneNumbers());
    }

    @Override
    public PhoneNumberDto getPhoneNumberById(Integer phoneNumberId) throws Exception {

        PhoneNumber phoneNumber = phoneNumberService.getPhoneNumberById(phoneNumberId);

        return toDto(phoneNumber);
    }

    @Override
    public List<PhoneNumberDto> getPhoneNumbersByCustomerId(Integer customerId) {

        List<PhoneNumber> customerNumbers = phoneNumberService.getPhoneNumbersByCustomerId(customerId);

        return toDto(customerNumbers);
    }

    @Override
    public PhoneNumberDto activatePhoneNumber(Integer phoneNumberId) throws Exception {

        PhoneNumber phoneNumber = phoneNumberService.activatePhoneNumber(phoneNumberId);

        return toDto(phoneNumber);
    }


    // ==== object to object mappers===

    public CustomerDto toDto(Customer customer) {

        CustomerDto customerDto = new CustomerDto();
        customerDto.setFirstname(customer.getFirstname());
        customerDto.setLastname(customer.getLastname());
        customerDto.setId(customer.getId());

        //todo add numbers
        List<PhoneNumberDto> phoneNumberDtos = new ArrayList<>();

        for (PhoneNumber p : customer.getPhoneNumbers()) {
            phoneNumberDtos.add(toDto(p));
        }

        customerDto.setPhoneNumbers(phoneNumberDtos);

        return customerDto;
    }

    public List<CustomerDto> toDtos(List<Customer> customers) {


        List<CustomerDto> customerDtos = new ArrayList<>();

        for (Customer p : customers) {
            customerDtos.add(toDto(p));
        }

        return customerDtos;
    }


    public PhoneNumberDto toDto(PhoneNumber phoneNumber) {

        PhoneNumberDto phoneNumberDto = new PhoneNumberDto();

        phoneNumberDto.setId(phoneNumber.getId());
        phoneNumberDto.setPhoneNumber(phoneNumber.getPhoneNumber());
        phoneNumberDto.setActivated(phoneNumber.getActivated());

        Customer customer = phoneNumber.getCustomer();
        if (customer != null) {
            phoneNumberDto.setCustomerId(customer.getId());
        }


        return phoneNumberDto;
    }

    public List<PhoneNumberDto> toDto(List<PhoneNumber> phoneNumbers) {


        List<PhoneNumberDto> phoneNumberDtos = new ArrayList<>();

        for (PhoneNumber p : phoneNumbers) {
            phoneNumberDtos.add(toDto(p));
        }

        return phoneNumberDtos;
    }
}

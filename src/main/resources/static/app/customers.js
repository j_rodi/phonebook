var customerModule = angular.module('customerModule',['uiSwitch']);


customerModule.controller('customerCtrl', function ($scope, $routeParams,$location, $http, services) {


    $scope.getCustomer = function getCustomer(customerId){

        $http.get('/customers/' + customerId)

            .success(function (data, status, headers, config) {

                $scope.customer = data;
            })
            .error (function (data, status, headers, config) {

                $scope.response = data.message;

                console.log(data);
            });
    };

    $scope.activatePhoneNumber = function activatePhoneNumber(phoneNumber){

        services.activatePhoneNumber(phoneNumber);
    };

    var customerId = $routeParams['customerId'];

    $scope.getCustomer(customerId);

});












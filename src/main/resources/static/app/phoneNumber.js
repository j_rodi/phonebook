var phoneNumberModule = angular.module('phoneNumberModule',['uiSwitch']);


/**
*  This controller is responsible for submitting a cramer CVC request to Minister.
* */
phoneNumberModule.controller('phoneNumberCtrl', function ($scope, $location, $http, services) {


    // Minister API call
    $scope.getPhoneNumbers = function getPhoneNumbers(){

        $http.get('/phoneNumbers')

            .success(function (data, status, headers, config) {

                $scope.phoneNumbers = data;
            })
            .error (function (data, status, headers, config) {

                $scope.response = data.message;

                console.log(data);
            });
    };

    $scope.activatePhoneNumber = function activatePhoneNumber(phoneNumber){

        services.activatePhoneNumber(phoneNumber);
    };

    $scope.getPhoneNumbers();



});


//defines a cramer cvcJob directive
phoneNumberModule.directive('phoneNumbersPanel', function() {
    return {
        restrict: 'AE',
        replace: 'true',
        templateUrl: 'template/view/phoneNumbers.html',
        controller: 'phoneNumberCtrl'

    };
});









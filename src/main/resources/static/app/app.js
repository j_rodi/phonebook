'use strict';

var app = angular.module('phoneBookApp', [
    'ngRoute',
    'ngAnimate',
    'dashboard',
    'angular.snackbar',
    'phoneNumberModule',
    'customerModule'
]);


/* Application route configs */
app.config(['$routeProvider', function ($routeProvider) {

    $routeProvider

        .when('/', {
            title: 'PhoneBookApp',
            templateUrl: 'template/view/dashboard.html',
            controller: 'dashboardCtrl'
        })

        .when('/customer/:customerId', {
            title: 'Minister',
            templateUrl: 'template/view/customer.html',
            controller: 'customerCtrl'
        })



        .otherwise({
            redirectTo: '/'
        });

    
}]);

app.factory("services", ['$http', function ($http) {



    var obj = {};


    obj.activatePhoneNumber = function (phoneNumber){

        $http.patch('/phoneNumbers/' + phoneNumber.id +  '/activate')

            .success(function (data, status, headers, config) {

                phoneNumber.activated = data.activated;
            })
            .error (function (data, status, headers, config) {

                $scope.response = data.message;

                console.log(data);
            });
    }


    return obj;

}]);

app.run(['$location', '$rootScope', function ($location, $rootScope) {
    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
        //  $rootScope.title = current.$route.title;
    });
}]);
